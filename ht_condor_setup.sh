#!/bin/bash

export HTCONDOR_EMAIL_ADDRESS="giovanni.guerrieri@cern.ch"

Help()
{
   # Display Help
   echo
   echo "Batch jobs submission aliases."
   echo
   echo "Options:"
   echo "- csub           Submit job, execute csub -h for more information"
   echo "- cjobs          Show ongoing tasks"
   echo "- cwatch         Toggle cjobs command (refreshes every 2 seconds)"
   echo "- cget <ID>      Get the log files of specific job or cluster"
   echo "- crm  <ID>      Remove specific job or cluster"
   echo "- crel <ID>      Release held job or cluster"
   echo "- cprio          Show user priority"
   echo "- chelp          Print this Help."
   echo
}

alias ctail="condor_tail -follow"
alias cssh="condor_ssh_to_job -auto-retry"
cstream()
{
   # Display stream
   #watch cat "$(readlink -f /proc/$@/fd/1)"
   cat "$(readlink -f /proc/$@/fd/1)"
}
# to print the Help
alias chelp=Help
# to check out priority for each user in the cluster
alias cprio="condor_userprio"
cget() {
   #RESET THE LAST OPTION PROCESSED
   OPTIND=1
   unset number

   while getopts :n: flag
   do
      case "${flag}" in
         n) number=${OPTARG};;
         :)  echo "missing mandatory argument!" >&2
               exit 1 ;;
         \?) echo "invalid option!" >&2
               exit 1 ;;
      esac
   done

   if [ ! -z "${number}" ]; then
      list=$(cjobs -format "%d "  ClusterId -constraint "ProcID==0")
      IFS=' ' read -r -a list_array <<< "$list"
      head_remove=$(expr ${#list_array[@]} - $number)
      list_array=("${list_array[@]:$head_remove}")
      echo "Getting the following jobs: ${list_array[@]}"
      condor_transfer_data ${list_array[@]}
   else
      condor_transfer_data $@
   fi
}
crm() {
   #RESET THE LAST OPTION PROCESSED
   OPTIND=1
   unset number

   while getopts :n: flag
   do
      case "${flag}" in
         n) number=${OPTARG};;
         :)  echo "missing mandatory argument!" >&2
               exit 1 ;;
         \?) echo "invalid option!" >&2
               exit 1 ;;
      esac
   done

   if [ ! -z "${number}" ]; then
      list=$(cjobs -format "%d "  ClusterId -constraint "ProcID==0")
      IFS=' ' read -r -a list_array <<< "$list"
      head_remove=$(expr ${#list_array[@]} - $number)
      list_array=("${list_array[@]:$head_remove}")
      echo "Getting the following jobs: ${list_array[@]}"
      condor_rm ${list_array[@]}
   else
      condor_rm $@
   fi
}

if [[ $HOSTNAME == *lxplus* ]]; then
   alias csub="source /afs/cern.ch/user/${USER:0:1}/${USER}/htcondor-atlas-udine/submit-job.sh"
   # to release held jobs
   alias crel="condor_release"
   # cjobs will be like our old bjobs
   # (add -allusers to see all running jobs)
   alias cjobs="condor_q"
   alias cwatch="watch condor_q"
   # command to remove a job (by also stopping it (?))
   #alias crm="condor_rm"
   # command to inspect the terminal output of the job while running
   alias cpeek="condor_tail -debug -auto-retry -follow"
   # to release held jobs
   alias crel="condor_release"
   

elif [[ $HOSTNAME == *farm*ts* ]]; then
   alias csub="source /gpfs/atlas/${USER}/htcondor-atlas-udine/submit-job.sh"
   export _condor_SCHEDD_HOST=sn9.ts.infn.it
   alias psub="condor_submit_dag"
   # cjobs will be like our old bjobs
   # (add -allusers to see all running jobs)
   alias cjobs="condor_q -name sn9.ts.infn.it"
   alias cwatch="watch condor_q -name sn9.ts.infn.it"
   # cqueues will be like our old bqueues
   # command to get log files
   #alias cget="condor_transfer_data -name sn9.ts.infn.it"
   # cget() {
   #    #RESET THE LAST OPTION PROCESSED
   #    OPTIND=1
   #    unset number

   #    while getopts :n: flag
   #    do
   #       case "${flag}" in
   #          n) number=${OPTARG};;
   #          :)  echo "missing mandatory argument!" >&2
   #                exit 1 ;;
   #          \?) echo "invalid option!" >&2
   #                exit 1 ;;
   #       esac
   #    done

   #    if [ ! -z "${number}" ]; then
   #       list=$(cjobs -format "%d "  ClusterId -constraint "ProcID==0")
   #       IFS=' ' read -r -a list_array <<< "$list"
   #       head_remove=$(expr ${#list_array[@]} - $number)
   #       list_array=("${list_array[@]:$head_remove}")
   #       echo "Getting the following jobs: ${list_array[@]}"
   #       condor_transfer_data -name sn9.ts.infn.it ${list_array[@]}
   #    else
   #       condor_transfer_data -name sn9.ts.infn.it $@
   #    fi
   # }
   # command to remove a job (by also stopping it (?))
   # alias crm="condor_rm -name sn9.ts.infn.it"

   # command to inspect the terminal output of the job while running
   alias cpeek="condor_tail -debug -name sn9.ts.infn.it -auto-retry -follow"

   # to submit any root macro as a c-job (use ...)
   alias cmacro='source /gpfs/atlas/pinamont/cmacro.sh'

   # to release held jobs
   alias crel="condor_release -name sn9.ts.infn.it"
fi