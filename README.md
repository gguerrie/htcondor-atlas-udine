**[Batch Jobs deployer](#batch-jobs-deployer)** |
**[Synopsis](#synopsis)** |
**[Submitting a Job](#submitting-a-job)**

# Disclaimer

This repository is inspired to the official [HTCondor documentation](https://htcondor.readthedocs.io/en/latest/) and to the [CERN tutorial](https://batchdocs.web.cern.ch/local/submit.html) so in order to have further clarifications, please feel free to explore it.

# Batch Jobs deployer

This deployer provides a mean to execute jobs on our beloved Trieste farm.

## Initial setup
Get the repository: 
```
git clone ssh://git@gitlab.cern.ch:7999/gguerrie/htcondor-atlas-udine.git
```
at the end of the file `~/.bashrc` put the command:
```
source <path-to-htcondor-atlas-udine>/ht-condor-setup.sh
```
Otherwise it is necessary to execute the above line by hand everytime a new session in the farm is initiated.

This will initialize the aliases to run the commands, a summary is shown below.<br />
- `csub`           Submit job, execute csub -h for more information<br />
- `cjobs`          Show ongoing tasks<br />
- `cwatch`         Toggle cjobs command (refreshes every 2 seconds)<br />
- `cget <ID>`     Get the log files of specific job or cluster<br />
    - `cget -n <number>`, meaning: "get the logs for the last `<number>` jobs."<br />
- `crm  <ID>`      Remove specific job or cluster<br />
    - `crm -n <number>`, meaning: "delete the last `<number>` jobs."<br />
- `ctail  <ID>`    Streams the output of the job<br />
- `crel <ID>`      Release held job or cluster<br />
- `cprio`          Show user priority<br />


## Synopsis
`csub -n <name of the job> [additional options]`
### Additional options
`-e`     Executable to pass (e.g. condor.sh) [**deprecated**]<br />
`-a`     Set alarm on job completion/failure <br />
`-c`     Command to pass (within "")<br />
`-m`     Explicitly set the necessary memory <br />
`-f`     Name of a file containing the commands to execute<br />
`-j`     Name of a file containing the commands to execute, at the beginnning of each line the name of the subjob is provided.<br />
`-a`     Set alarm on job completion/failure (Please set up your email address at Line 3 of ht_condor_setup.sh)<br />
`-l`     Set the max walltime at 'tomorrow' <br />
`-h`     Print the Help<br />

**WARNING**: all the options are mutually exclusive, except for `-m`.



# Submitting a Job
This deployer embeds three different ways to execute a set of jobs:
1.  Submit a `.sub` file: in this case it's just `csub -n <name> <file.sub>` 
2.  Via an executable file (option `-e`)
3.  Via command line. (option `-c`)
4.  Via a text file containing the commands to run. (option `-f` or `-j`)

## 1. Executable File [**deprecated**]
Execute the content of a specific `.sh` file, possibily located in the `./` directory.

**EXAMPLE**: `csub -n test -e condor.sh`<br />
**WARNING**: kind of useless, it's better the `-c` option :D

## 2. Command Line
Execute a specific command submitted inline.

**EXAMPLE**: `csub -n test -c "source setup.sh"`
Please notice that it is possible to input multiple bash commands by doing as follows: 
```csub -n test -c "source setup.sh && mkdir -p test/folder && <other operations>"
```

## 3. Textfile
Interprets as commands each line of an input textfile, creating a set of jobs running in the same batch.

**EXAMPLE**: suppose to have a file named `test.txt`, whose content is
```
CreateMiniNtuples --configFile="testConfig.cfg"
CreateMiniNtuples --configFile="config1l.cfg"
CreateMiniNtuples --configFile="config2l.cfg"
CreateMiniNtuples --configFile="configB2k.cfg"
```
By executing `csub -n test -f test.txt`, 4 jobs will be created **under the same job ID**, running each one of the lines contained in the `.txt` file in parallel.
Notice that if not specified, each subjob will acquire as a suffix a numeric counter starting from 0.

**EXAMPLE**: job name: `test`, subjob names: `test_0, test_1, test_2, test_3`.

### Option `j`
Use the `j` option when submitting to allow the user to use a specific string as suffix, instead of a number. The syntax to adopt is shown in the example below.

**EXAMPLE**: suppose to have a file named `test.txt`, whose content is
```
testConfig,CreateMiniNtuples --configFile="testConfig.cfg"
config1l,CreateMiniNtuples --configFile="config1l.cfg"
config2l,CreateMiniNtuples --configFile="config2l.cfg"
configB2k,CreateMiniNtuples --configFile="configB2k.cfg"
```
With this option, a job named `test` will result in the following subjobs: `test_testConfig, test_config1l, test_config2l, test_configB2k`

## 4. Arguments [**deprecated**]
This option can be used in combination with the `-c` one to append arguments or additional operations to the original submission command.

**EXAMPLE**: `csub -n test -c "source setup.sh;" -a "mkdir -p test/folder"`<br />
**WARNING**: kind of useless, it's better this: `csub -n test -c "source setup.sh && mkdir -p test/folder && <other operations>"` :D







