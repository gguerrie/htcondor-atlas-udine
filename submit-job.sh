#!/bin/bash

# --------------------------
# Usage:
#    - For submitting a job, call this executable script:
#       ./submit-job <name> <executable> [arguments]
#    where:
#       * <name> is the chosen name of the job, used as base-name for the submission file (created on-the-fight) and for the log-files
#       * <executable> is the script containing the command to run (it has to be an actual executable file, cannot be directly a command)
#       * [arguments] optional set of arguments to be passed to the executable script
# --------------------------

Help()
{
   # Display Help
   echo
   echo "Batch jobs submission script."
   echo
   echo "Syntax: csub [-n|-c|-m|-h]"
   echo "options:"
   echo "-n     Name of the job to submit"
   echo "-e     Executable to pass (e.g. condor.sh)"
   echo "-c     Command to pass (within \"\")"
   echo "-m     Bigmem option: RequestMemory = 16108"
   echo "-p     Just the submission file is produced (for DAG jobs)"
   echo "-f     Name of a file containing the commands to execute"
   echo "-j     Enables subjobs suffix labels, according to the correct syntax of input file"
   echo "-a     Set alarm on job completion"
   echo "-l     Set the max walltime at 'tomorrow'"
   echo "-h     Print this Help."
   echo "DO NOT use the options -e and -f together."
   echo
}
#RESET THE LAST OPTION PROCESSED
OPTIND=1
unset args
unset alarm
unset jobName
unset filename
unset namelist
unset requirement


parallel=0
long=0
alarm=0

while getopts n:c:e:f:r:j:m:p:hla flag
do
    case "${flag}" in
		a) alarm=1;;
        n) export jobName=${OPTARG};;
		m) mem=${OPTARG} ;;
		l) long=1;;
		p) parallel=1;;
		c) args=${OPTARG} ;;
        e) export command=${OPTARG};;
        f) filename=${OPTARG};;
		j) namelist=${OPTARG};;
		r) requirement=${OPTARG};;
		h) Help
		   exit;;
		:)  echo 'missing mandatory argument!' >&2
            exit 1 ;;
		\?) echo 'invalid option!' >&2
            exit 1 ;;
		
    esac
done

echo $args
#echo $3

#export here=`pwd`
export here=$(realpath ".")

if [ -d "$here/condor/log" ]; then
  ### Take action if log exists ###
  echo "INFO: log folder ok"
else
  ###  Control will jump here if $DIR does NOT exists ###
  mkdir -p $here/condor/log
fi

if [ -d "$here/condor/out" ]; then
  ### Take action if log exists ###
  echo "INFO: out folder ok"
else
  ###  Control will jump here if $DIR does NOT exists ###
  mkdir -p $here/condor/out
fi

if [ -d "$here/condor/err" ]; then
  ### Take action if log exists ###
  echo "INFO: err folder ok"
else
  ###  Control will jump here if $DIR does NOT exists ###
  mkdir -p $here/condor/err
fi

if [[ $HOSTNAME == *lxplus* ]]; then
	export submitCommand="condor_submit"
else
	export submitCommand="condor_submit -name sn9.ts.infn.it -spool "
fi

echo "==> Submitting local job \"$jobName\"" : $command" "$arguments

if [[ $3 == *sub ]]
then
	echo $3
	eval $submitCommand $3
fi

subFile="$here/condor/log/${jobName}.sub"

condor="$here/condor/log/condor.sh"
echo "#!/bin/bash"         	> $condor
echo "cd $here"			   	>> $condor
echo "echo \"\$@\""            		>> $condor
echo "eval \"\$@\""            		>> $condor

# Start building up the submission file with common fields
echo "# Welcome to the condor file :D"		            		>  $subFile
echo "getenv	 = True" 			   							>> $subFile
###############################################################################
if [ ! -z ${filename} ] # File with rows set up as commands
then
	echo "Executable = $here/condor/log/condor.sh"            		>> $subFile
	echo "Arguments  = \$(state)"         							>> $subFile
	echo "Error      = $here/condor/err/${jobName}_\$(ProcID).err" 	>> $subFile
	echo "Output     = $here/condor/out/${jobName}_\$(ProcID).out" 	>> $subFile
	echo "Log        = $here/condor/log/${jobName}_\$(ProcID).log" 	>> $subFile

###############################################################################
elif [ ! -z ${namelist} ] # File with rows set up as name,command
then
	echo "Executable = $here/condor/log/condor.sh"            		>> $subFile
	echo "Arguments  = \$(state)"         							>> $subFile
	echo "Log		 = $here/condor/log/${jobName}_\$(idname).log" 	>> $subFile
	echo "Error      = $here/condor/err/${jobName}_\$(idname).err" 	>> $subFile
	echo "Output     = $here/condor/out/${jobName}_\$(idname).out" 	>> $subFile

###############################################################################
elif [ ! -z ${command} ]
then
	echo "Executable = $command"            						>> $subFile
	echo "Log      	 = $here/condor/log/${jobName}.log" 			>> $subFile
	echo "Error      = $here/condor/err/$jobName.err" 				>> $subFile
	echo "Output     = $here/condor/out/$jobName.out" 				>> $subFile

###############################################################################
elif [ ! -z "${args}" ]
then
	echo "Executable = $here/condor/log/condor.sh"            		>> $subFile
	echo "Arguments  = ${args}"         							>> $subFile
	echo "Error      = $here/condor/err/${jobName}.err" 			>> $subFile
	echo "Output     = $here/condor/out/${jobName}.out" 			>> $subFile
	echo "Log        = $here/condor/log/${jobName}.log" 			>> $subFile
fi
###############################################################################
################			  Additional Options			 ##################
###############################################################################
if [ ! -z $mem ] 
then
	echo "RequestMemory	= $mem"		   							>> $subFile
	#echo "RequestMemory	= 6000"		   							>> $subFile
fi

if [[ $HOSTNAME == *lxplus* ]]; then
	if [ "$long" = '1' ] 
	then
		echo "+JobFlavour = \"tomorrow\""									>> $subFile
	else
		echo "+JobFlavour = \"longlunch\""									>> $subFile
	fi
fi

if [ "$alarm" = '1' ]; then
	echo "notification = Always"									>> $subFile
	echo "notify_user = $HTCONDOR_EMAIL_ADDRESS"					>> $subFile
fi

echo ${requirement}
if [ ! -z "${requirement}" ]
then
	echo "Requirements = $requirement"								>> $subFile
fi

echo "max_retries             = 10"									>> $subFile
echo "requirements = Machine =!= LastRemoteHost"					>> $subFile
#echo "+JobBatchName	= \"$ClusterID\" " 								>> $subFile

if  [ ! -z ${command} ] || [ ! -z "${args}" ]
then
	echo "Queue"                           							>> $subFile
elif [ ! -z ${filename} ]
then
	echo "queue state from $filename"                         >> $subFile
elif [ ! -z ${namelist} ]
then
	echo "queue idname,state from $namelist"                  >> $subFile
fi
	
if [ -z ${jobName} ] && [[ $3 == *sub ]]
then 
	echo "ERROR: option -n is mandatory."
elif [ ! -z ${jobName} ] && [ "$parallel" = '1' ]
then
	export PARALLEL_SUBFILE="${subFile}"
	echo "INFO: saved the subfile at ${subFile}"
elif [[ $3 != *sub ]]
then
	eval $submitCommand $subFile
fi